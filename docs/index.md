# Inputs and Outputs: An introduction to code and electronics

![](assets/batt_fan.png)

## SHORT DESCRIPTION

This module  will help you forget everyday technology is a **black box**. We will look at the basic principles of hardware and software, or in other words, electronics and coding.

The main objective is to provide essential tools and concepts to help participants navigate the enormous amount of online resources available to materialize ideas using software and hardware.

## MODULES

![](assets/our_plan.png)

[**Volts, Watts and waterfalls**](volts_waterfalls.md)  
_How does our home electricity works, why our cellphone needs an adapter, and how to use a multimeter?_

[**From physics to information**](information_theory.md)  
_Computers are much more than laptops and mobile phones: how to begin with Arduino and when to use it_

[**From ideas to code**](from_ideas_to_code.md)  
_Breaking myths about programming languages: telling an Arduino what to do_

[**Hello World! - Hands-on**](hello_world.md)  
_Let's build out first --not so-- smart thing_

![](https://hackster.imgix.net/uploads/attachments/865224/2z2js2_3edJ2wegi7.gif)

## TECHNICAL REQUIREMENTS

### Workshop Electronics Kit

You should have received something like this:

![](assets/kit.jpg)

**What's inside?**

- 1 x [Barduino 4.0](https://gitlab.com/fablabbcn-projects/electronics/barduino)
- 1 x [Electronics kit](https://www.amazon.es/Electr%C3%B3nicos-Resistencias-Condensadores-Potenci%C3%B3metro-Especificaciones/dp/B01MRIG6YM?ref_=ast_sto_dp)
- 1 x Micro USB C
- 1 x [SW2813 LED Strip](https://www.mouser.es/ProductDetail/Seeed-Studio/104020109?qs=byeeYqUIh0PmEriTC2u0yQ%3D%3D)

### Laptop Requirements

Any Linux, Windows or OSX laptop will do.

### Software Requirements

- Arduino IDE (Libre software) - download [here](https://www.arduino.cc/en/Main/Software#download)
