# From Ideas to Code

## Coding languajes
> A **high-level** programming language is a programming language with strong abstraction from the details of the computer.  
> A **low-level** programming language is a programming language that provides little or no abstraction from a computer's instruction set architecture.

_From_ [_wikipedia_](https://en.wikipedia.org/wiki/High-level_programming_language)

<div style="text-align:center">
<img src="https://miro.medium.com/max/1200/1*8j2PmhExz4q87OoddaH7ag.png" width=400>
</div><br>

## Algorithms and flow charts

> [...] an algorithm is a finite sequence of well-defined, computer-implementable instructions, typically to solve a class of problems or to perform a computation. Algorithms are always **unambiguous** [...]  


To represent algorithms we can use a for of diagram called **flow chart**.

> A flowchart is a type of diagram that represents a workflow or process. A flowchart can also be defined as a diagrammatic representation of an algorithm, a step-by-step approach to solving a task.  

> The flowchart shows the steps as boxes of various kinds, and their order by connecting the boxes with arrows. This diagrammatic representation illustrates a solution model to a given problem. Flowcharts are used in analyzing, designing, documenting or managing a process or program in various fields.

_From_ [_wikipedia_](https://en.wikipedia.org/wiki/Flowchart)

<div style="text-align:center">
<img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/examples/flowchart-templates/simple-flowchart.svg" width=400>
</div><br>

There are a lot of different **symbols** you can use on your flowcharts, these is the basic set and its function:

<div style="text-align:center">
<img src="../assets/fc_symbols.jpg">
</div><br>

!!! Note "References"
	- You can draw your flow charts online with [draw.io](https://draw.io)

### I/O Programming

Our code will interface with the outer world, receiving information, i.e. **_inputs_**, and actuating in some way, i.e. **_outputs_**. After receiving information from the environment, normally through some kind of sensor, we **_process_** the collected data to take decisions and actuate.

![](assets/ipo.png)

A balancing robot is a good example of this technic.

![](assets/input_outputs_balancer.png)


## Practical coding

### Variables

Think of variables as virtual drawers where you can store your stuff!

<div style="text-align:center">
<img src="../assets/drawers.jpg" width=300>
</div><br>

You can even label them with names, so you don't forget whats inside. But remember to tell the computer what the content will be, so she can build the drawer big enough to fit it. 

```
bool hello;
byte my;
int friend;
float how;
long are;
char you;
String today; // Only arduino!
```

### Coding control flow statements

We can use control flow statements to manage how our program reacts to different inputs and actuates on the outputs.

Conditionals help us take decitions, remember that your question can only be answered with _true_ or _false_.

```
if (condition) {

    // Do stuff

}
```

We can chain many conditions one after the other:

```
if (condition) {

	// Do stuff
}

else if (condition2) {
	
	// Do stuff 2

} 

else {

	// Do other stuff

} 

```

Loops can be used to repeat operations based on diferent parameters:

```
while (condition) {

    // Do stuff

}
```

```
for (# iterations) {

	// Do stuff

}
```

### Comments

It is easy to forget what a piece of code was supposed to do if we read it after a couple of days, to write reminders or to explain what the code does, or simply to make a joke, we can use **_comments_**. In C language, simply starting a line with _//_ will make the compiler ignore all the content on it.

```
// This line will be ignored by the computer!!

if (fail) {

	// Failure is the way to learn...
	print("ERROR!!!");

}

```

### Functions

When our code starts growing, it can get very messy and difficult to understand. Functions are a way to organize our code by grouping instructions that we use often and then calling them. We can assign any name we want to functions, the idea is making it descriptive so we remember what the function does.

![](https://docs.arduino.cc/static/f98f8f8864dbc20a93f66665e128aecf/6c1e7/FuncAnatomy.png)

```
// We define a simple function to temporary light a led
void blink() {
	digitalWrite(LED_PIN, HIGH);
	delay(500);
	digitalWrite(LED_PIN, LOW);
}

// And we can use it whenever we want!
void loop() {
	if (ERROR) {
			blink();
	} 
}
```

### Libraries

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Duke_Humfrey%27s_Library_Interior_6%2C_Bodleian_Library%2C_Oxford%2C_UK_-_Diliff.jpg/1200px-Duke_Humfrey%27s_Library_Interior_6%2C_Bodleian_Library%2C_Oxford%2C_UK_-_Diliff.jpg)

**Libraries** are sets of tools that someone has built to make certain tasks easier, for instance, reading pins in a microcontroller, or controlling the LEDs. For using our LED strip, we will need to install one library.

!!! Note "References"
	* [Using Variables in Sketches](https://docs.arduino.cc/learn/programming/variables)
	* Arduino [article](https://docs.arduino.cc/learn/programming/functions) on functions.
	* [Get to know Arduino Libraries](https://docs.arduino.cc/learn/starting-guide/software-libraries)
	* You can find alot more info on the arduino [learn](https://docs.arduino.cc/learn) site.

## Arduino programming

### Installation

You will need to install the [Arduino IDE](https://www.arduino.cc/en/Main/Software). After that, we need to define the board that we are going to use.

!!! Note "Guide"
    To get started with the Arduino IDE, and install the boards needed for the Adafruit FEATHER32 board, please:

    Open your Arduino. Go to: `Preferences > Additional Board Manager URLs`

    ![](assets/preferences.png)

    There, add the boards sources. **One line per URL**:

    ![](assets/boards_json.png)

    Paste this in the field there:

    ```
    https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
    ```


!!! Note "Drivers"

    Install the drivers too from here: [https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/using-with-arduino-ide](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/using-with-arduino-ide)

### Pins

The input/output pins _I/O pins_ of our board are the gates that we use to interact with the real world. To know with who are we dealing we need to know the [pinout](https://en.wikipedia.org/wiki/Pinout) of our board.

![](assets/Pinout.svg)

!!! Note "Three things to remember"
    [Link to Barduino WIP documentation](https://fablabbcn-projects.gitlab.io/electronics/barduino-docs/)




### Hands on

!!! Note "Three things to remember"
    1. Comming in or going out? [pinMode()](https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/)
    2. Pin on/off with [digitalWrite()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/)
    3. Read button state with [digitalRead()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/)

#### Reading a button

Digital input with [DigitalRead()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/)

![](assets/07-digitalRead_bb.jpg)

~~~arduino
int buttonPin = 2;
int value = 0;

void setup() {
  pinMode(buttonPin, INPUT);
  Serial.begin(115200);
}

void loop() {
  value = digitalRead(buttonPin);
  Serial.println(value);
}
~~~

#### Blinking led

Digital output with [digitalWrite()](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/) using [delay](https://www.arduino.cc/reference/en/language/functions/time/delay/) function.

![](assets/06-digitalWrite_bb.jpg)

```arduino
void setup() {
    // Set pin 9 as output
  pinMode(9, OUTPUT);
}

// Loop runs forever
void loop() {
    // Voltage up in pin 9
    digitalWrite(9, HIGH);
    // Wait 200ms with pin 9 HIGH
    delay(200);
    // Voltage down in pin 9
    digitalWrite(9, LOW);
    // Wait 200ms with pin 9 LOW
    delay(200);    
}
```

#### I/O
**If you are feeling adventurous, try this**

![](assets/08-digitalRead&Write_bb.jpg)

```
// Our variable for checking if it's pressed or not
bool pressed = false;

// the setup function runs once when you press reset or power the board
void setup() {

  pinMode(9, OUTPUT);
  pinMode(2, INPUT);
  // Turn the LED off
  digitalWrite(9, LOW);
}

// Loop runs forever
void loop() {
  // Read the pin
  if (digitalRead(2)) {
    // pressed!
    pressed = false;
  } else {
    // not pressed!
    pressed = true;
  }

  if (pressed) {
    // Turn it on
    digitalWrite(9, HIGH);
  } else {
    // Turn it off
    digitalWrite(9, LOW);
  }
}
```

!!! Note "General resources"
	- [hackaday.com](https://hackaday.com/) is one of the best blogs on DIY inventions and hardware hacking
	- [lowtechmagazine.com](https://www.lowtechmagazine.com/) many technology choices are political and economic, looking at past forgotten technologies helps us see the future
	- [archive.fabacademy.org](http://archive.fabacademy.org/) 10 years of project from Fab Labs around the world. Sometimes hard to browse but inspiring!
	- [learn.adafruit.com](https://learn.adafruit.com/) a really good site for electronics and programming tutorials, especially for beginners
	- [instructables](https://learn.adafruit.com/) more and more DIY tutorials, sometimes aren't good but there's a lot
    - [REPL.it](https://repl.it/languages)
    - [Instructions for the Apollo Guidance Computer](https://en.wikipedia.org/wiki/Apollo_Guidance_Computer#Instruction_set)
