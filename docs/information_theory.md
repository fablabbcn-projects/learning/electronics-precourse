# From physics to information

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/p0ASFxKS9sg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Getting inspired by the past

[![](assets/computer.png)](http://linkedbyair.net/bin/Ted%20Nelson%20Computer%20Lib%20Dream%20Machines%20-%201st%20edition%201974.pdf)  
_Click on the image to download the PDF_  

!!! Warning "Some references"
    Do not try to watch them all! Simply choose a topic you like and jump over the videos. Maybe you find something interesting and suddenly want to learn more about it.

    * [Information Theory](https://www.youtube.com/watch?v=p0ASFxKS9sg&list=PLP6PHJ8SLR6D4ytpHhZBdylPNcazU5m7o)
    * [More on information Theory and Coding (for those who enjoyed the videos above)](https://www.youtube.com/playlist?list=PLzH6n4zXuckpKAj1_88VS-8Z6yn9zX_P6)
    * [How computers think](https://www.youtube.com/watch?v=dNRDvLACg5Q)
    * [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
    * [How computers work](https://www.youtube.com/watch?v=nN9wNvEnn-Q)
    * [How computer memory works the insides](https://www.youtube.com/watch?v=XETZoRYdtkw&t=6s)
    * [Colossus the first electronic computer](https://www.youtube.com/watch?v=knXWMjIA59c)
    * [How they design the computers they landed astronauts to the moon](https://www.youtube.com/watch?v=xQ1O0XR_cA0)
    * [UNIX or what an operative system does](https://www.youtube.com/watch?v=tc4ROCJYbm0)
    * [Looking at how things are built and work to learn how to design](https://www.youtube.com/playlist?list=PLvOlSehNtuHsy89OdSxBajult8e5srVLA)
    * [An introduction to Arduino towards future assignments](https://www.youtube.com/watch?v=_h1m6R9YW8c)
    * [The Secret Life of Machines](https://www.youtube.com/watch?v=KDpNQQqdSh8&list=PLByTa5duIolYRtq45Cz_GmtzfWJyA4bik)

## Representing information

Now, let's start representing information with our very basic circuit.

![](assets/multimeter.png)

!!! Note "A **couple of questions**"
    - How much information are we able to represent with this circuit?
    - How many possibilities do we have? And what if we combine all the circuits we just built?

### Digital Logic in an Analog World

As we saw before:

> A single switch can be on or off, enabling the storage of 1 bit of information. Switches can be grouped together to store larger numbers. This is the key reason why binary is used in digital systems.

<div style="text-align:center">
<img src="https://usercontent1.hubstatic.com/13809446.gif" width="400"></div><br>

With a bank of eight switches we can _store_ 2^8^ = 256 possible numbers. Each of those switches is called **1 bit**, and 8 of them are a **byte**: 

<div style="text-align:center">
<img src="https://usercontent2.hubstatic.com/8735065_f520.jpg" width="600"></div><br>

This implies then, that to represent information with electricity, we have one very important limitation: information becomes discrete:

<div style="text-align:center">
<img src="https://i.imgur.com/9D2GKW6.png" width="600"></div><br>

Of course, depending on the amount of bits we use we have a more precise curve:

<div style="text-align:center">
<img src="http://pediaa.com/wp-content/uploads/2015/08/Difference-Between-Analog-and-Digital-Signals-A2D_2_bit_vs_3_bit.jpg" width="600"></div><br>

And if we put enough of them, we can represent more complex things like this:

<div style="text-align:center">
<img src="https://i.imgur.com/4MAPJzp.png" width="600"></div><br>

**Logic Levels**

Now that we have talked about representing information with single bit, we have to agree on how things are going to talk to one another. For this, we standardise what we consider as a **1** (high voltage) and **0** or low voltage. We call this **Logic levels**:

<div style="text-align:center">
<img src="https://i.imgur.com/zG4NPwb.png"></div><br>

### The core component: Transistor

Wouldn't it be ideal if we had a tiny mini switch we could control by ourselves? The transistor is nothing else than an electronic switch.

<div style="text-align:center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/BJT_symbol_NPN.svg/1200px-BJT_symbol_NPN.svg.png" alt="BJT symbol NPN.svg" width=200></div><br>

And it basically works like this:

<div style="text-align:center">
<img src="https://www.build-electronic-circuits.com/wp-content/uploads/2014/05/transistor-current-explanation.png" width=400></div><br>

We will see how this transitor helps us build more complex things, by controlling how they are connected together.

!!! Note "Reference Tutorials"
    - [Binary](https://learn.sparkfun.com/tutorials/binary)
    - [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
    - [Logic Levels](https://learn.sparkfun.com/tutorials/logic-levels/all)
    - [Guerrilla electronics](http://lcamtuf.coredump.cx/electronics/)

## Putting things together: circuits

<div style="text-align:center">
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.makerspaces.com%2Fwp-content%2Fuploads%2F2017%2F05%2Fled-circuit-breadboard-800x474.jpg&f=1" width=600></div><br>

<div style="text-align:center">
<img src="https://www.autodesk.com/products/eagle/blog/wp-content/uploads/2017/02/LED-circuit.png" width=600></div>

![](assets/breadboard_complex.jpg)

![](assets/circuit.png)

### Combinational vs sequential

Circuits can be [**combinational**](https://en.wikipedia.org/wiki/Combinational_logic), when they don't have any clue about their own state (like a mechanism):

<div style="text-align:center">
<img src="http://2.bp.blogspot.com/_11-1LH0aw4k/SkyxdsoJKZI/AAAAAAAAAAs/F2UGFxSJAEo/w1200-h630-p-nu/dld+combinational.bmp" width=500></div><br>

Here you have them being used in a Marshall Amplifier:

<div style="text-align:center">
<img src="https://www.gearslutz.com/board/attachments/so-many-guitars-so-little-time/382906d1391228908-low-wattage-marshall-style-amp-image_1180.jpg" width=500></div><br>

But they can also be [**sequential**](https://en.wikipedia.org/wiki/Sequential_logic), when they are _aware_ of their state:

<div style="text-align:center">
<img src="https://i.imgur.com/1eOZden.png" width=500></div><br>

In **combinational logic**, the output is a function of the present inputs only. The output is independent of the previous outputs.

**Sequential logic** is the form of Boolean logic where the output is a function of both present inputs and past outputs. In most cases, the output signal is fed back into the circuit as a new input. Sequential logic is used to design and build finite state machines.

<div style="text-align:center">
<img src="https://i.ytimg.com/vi/JQ0JOfxUAgY/maxresdefault.jpg" width=500></div><br>

Truth table for 1-bit Full Adder (above):

<div style="text-align:center">
<img src="https://codestall.files.wordpress.com/2017/06/truthtable_1bit_adder_yz.png?w=342&h=229"></div><br>

### Hands on

Let's make these first combinational circuits (choose one or both):

**AND gate**

![](assets/circuit_03_and.png)

**OR gate**

![](assets/circuit_03_or.png)

## Going integrated

The Apollo Guidance Computer:

![](http://pop.h-cdn.co/assets/17/11/1489433744-screen-shot-2017-03-13-at-33525-pm.png)

With some integrated circuits in flat-pack design:

<div style="text-align:center">
<a href="https://commons.wikimedia.org/wiki/File:Agc_flatp.jpg#/media/Archivo:Agc_flatp.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/1/13/Agc_flatp.jpg" alt="Agc flatp.jpg"></a></div><br>

Now IC's come in tons of flavours:

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/a/9/8/b/8/51c1ea70ce395f5f0d000000.jpg">
</div><br>

![](https://cdn.sparkfun.com/r/600-600/assets/7/a/6/9/c/51c0d009ce395feb33000000.jpg)

<div style="text-align:center">
<iframe width="600" height="400" src="https://www.youtube.com/embed/Fxv3JoS1uY8" frameborder="0" allow="autoplay; encrypted-media; picture-in-picture" allowfullscreen></iframe></div>

And then they become **programmable**:

![](assets/bugs.jpg)

Early computing machines were programmable in the sense that they could follow the sequence of steps they had been set up to execute, but the "program", or steps that the machine was to execute, were set up usually by changing how the wires were plugged into a patch panel or plugboard.

A **stored-program computer** is a computer that stores program instructions in electronic memory.

![](assets/stored.jpg)
_By Parrot of Doom - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=8318196_

A microcontroller (MCU for microcontroller unit) is a small computer on a single integrated circuit. A microcontroller contains one or more CPUs (processor cores) along with memory and programmable input/output peripherals. In 1974 Intel released the first microcontroller, the Intel 4004:

<div style="text-align:center">
<a href="https://commons.wikimedia.org/wiki/File:Intel_C4004.jpg#/media/File:Intel_C4004.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Intel_C4004.jpg/1200px-Intel_C4004.jpg" alt="Intel C4004.jpg" width=600></a>
</div><br>

It was a revolution (specially for printers :joy:):

<div style="text-align:center">
<img src="http://2.bp.blogspot.com/-_RtOTJIuT2g/TsUy9TI6itI/AAAAAAAAAS4/lfWcoTSIdEc/s1600/intel+4004+1971.jpg" width=600>
</div><br>

And from them on:

<div style="text-align:center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Transistor_Count_and_Moore%27s_Law_-_2011.svg/1200px-Transistor_Count_and_Moore%27s_Law_-_2011.svg.png" width=600>
</div><br>

## The Arduino project

<div style="text-align:center">
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fyt3.ggpht.com%2F-XW1x_JT1g24%2FAAAAAAAAAAI%2FAAAAAAAAAAA%2FlMWjeVh_HEw%2Fs900-c-k-no%2Fphoto.jpg&f=1" width=200>
</div>

<div style="text-align:center">
<img src="https://i.imgur.com/sEQcWuF.png1">
</div>

![](https://hackster.imgix.net/uploads/attachments/907564/dsc01880_aCYwk124ES.JPG?auto=compress&w=900&h=675&fit=min&fm=jpg)
> The importance of the freedom to use, understand, modify and share your tools.

* Hardware - Programming IDE and libs - Community
![](assets/ardcom.png)

!!! Note "Resources"
    - [Arduino](https://arduino.cc)
    - [Fritzing](http://fritzing.org/home/)
    - [Arduino reference](https://www.arduino.cc/reference/en/)
    - [An introduction to Arduino](https://www.youtube.com/watch?v=_h1m6R9YW8c)
